# firebase_test

A simple Flutter project for firebase integrations.

# Getting Started
## Firebase configuration
- Install firebase cli. [Firebase cli installation guide](https://firebase.google.com/docs/cli)
- Install `flutterfire` [flutterfire installation guide](https://firebase.flutter.dev/docs/cli)
- run `firebase login`
- run `flutterfire configure`


## Install dependencies
- run `flutter pub get`
- then `flutter run` to run app device or emulators

## Firebase console configuration
- Need to enable firebase auth for google and email login
- Need to add fingerprints on projects settings
- To get fingerprint type `cd android && .\gradlew signingreport` from project root directory
- Disable `Email enumeration protection` from authentication > settings > user actions.
- Enable App Check for app.