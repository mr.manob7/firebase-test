import 'package:firebase_test/pages/sign_up/cubit/sign_up_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:formz/formz.dart';

class SignUpForm extends StatelessWidget {
  const SignUpForm({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocListener<SignUpCubit, SignUpState>(
        listener: (context, state) {
          if (state.status.isSuccess) {
            Navigator.of(context).pop();
          } else if (state.status.isFailure) {
            ScaffoldMessenger.of(context)
              ..hideCurrentSnackBar()
              ..showSnackBar(
                SnackBar(
                    content: Text(state.errorMessage ?? 'Sign Up Failure')),
              );
          }
        },
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Container(
                height: 64,
                padding: const EdgeInsets.only(left: 10),
                color: Colors.transparent,
                child: Row(
                  children: [
                    IconButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        icon: const Icon(Icons.arrow_back_ios_new))
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    const Padding(
                      padding: EdgeInsets.all(20),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Let's create",
                            style: TextStyle(
                                fontSize: 30, fontWeight: FontWeight.bold),
                          ),
                          Text("Your Account", style: TextStyle(fontSize: 30))
                        ],
                      ),
                    ),
                    _EmailInput(),
                    const SizedBox(height: 8),
                    _PasswordInput(),
                    const SizedBox(height: 8),
                    _ConfirmPasswordInput(),
                    const SizedBox(height: 8),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        _terms(),
                        Expanded(
                          child: RichText(
                              text: const TextSpan(
                                  text: "Read our ",
                                  style: TextStyle(color: Colors.black),
                                  children: [
                                TextSpan(
                                    text: "Privacy Policy.",
                                    style:
                                        TextStyle(fontWeight: FontWeight.bold)),
                                TextSpan(
                                    text:
                                        " Tap \"Agree and continue\" to accept the "),
                                TextSpan(
                                    text: "Terms Of Service.",
                                    style:
                                        TextStyle(fontWeight: FontWeight.bold))
                              ])),
                        )
                      ],
                    ),
                    const SizedBox(height: 8),
                    _SignUpButton(),
                  ],
                ),
              )
            ],
          ),
        ));
  }
}

class _terms extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SignUpCubit, SignUpState>(
        buildWhen: (previous, current) =>
            previous.termsAccepted != current.termsAccepted,
        builder: (context, state) {
          return Checkbox(
              value: state.termsAccepted,
              onChanged: (bool? value) {
                context.read<SignUpCubit>().termsAccepted(value ?? false);
              });
        });
  }
}

// Read our Privacy Policy. Tap "Agree and
// continue" to accept the Terms Of Service.
class _EmailInput extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SignUpCubit, SignUpState>(
      buildWhen: (previous, current) => previous.email != current.email,
      builder: (context, state) {
        return TextField(
          key: const Key('signUpForm_emailInput_textField'),
          onChanged: (email) => context.read<SignUpCubit>().emailChanged(email),
          keyboardType: TextInputType.emailAddress,
          decoration: InputDecoration(
            labelText: 'Email address',
            helperText: '',
            errorText:
                state.email.displayError != null ? 'invalid email' : null,
          ),
        );
      },
    );
  }
}

class _PasswordInput extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SignUpCubit, SignUpState>(
      buildWhen: (previous, current) =>
          (previous.password != current.password) ||
          (previous.passwordVisibility != current.passwordVisibility),
      builder: (context, state) {
        return TextField(
          key: const Key('signUpForm_passwordInput_textField'),
          onChanged: (password) =>
              context.read<SignUpCubit>().passwordChanged(password),
          obscureText: !state.passwordVisibility,
          decoration: InputDecoration(
              labelText: 'Password',
              helperText: '',
              errorText: state.password.displayError != null
                  ? 'invalid password'
                  : null,
              suffixIcon: IconButton(
                icon: Icon(state.passwordVisibility
                    ? Icons.visibility_off
                    : Icons.visibility),
                onPressed: () {
                  context.read<SignUpCubit>().togglePasswordVisibility();
                },
              )),
        );
      },
    );
  }
}

class _ConfirmPasswordInput extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SignUpCubit, SignUpState>(
      buildWhen: (previous, current) =>
          previous.password != current.password ||
          previous.confirmedPassword != current.confirmedPassword ||
          previous.passwordVisibility != current.passwordVisibility,
      builder: (context, state) {
        return TextField(
          key: const Key('signUpForm_confirmedPasswordInput_textField'),
          onChanged: (confirmPassword) => context
              .read<SignUpCubit>()
              .confirmedPasswordChanged(confirmPassword),
          obscureText: !state.passwordVisibility,
          decoration: InputDecoration(
              labelText: 'Confirm password',
              helperText: '',
              errorText: state.confirmedPassword.displayError != null
                  ? 'passwords do not match'
                  : null,
              suffixIcon: IconButton(
                icon: Icon(state.passwordVisibility
                    ? Icons.visibility_off
                    : Icons.visibility),
                onPressed: () {
                  context.read<SignUpCubit>().togglePasswordVisibility();
                },
              )),
        );
      },
    );
  }
}

class _SignUpButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SignUpCubit, SignUpState>(
      builder: (context, state) {
        return state.status.isInProgress
            ? const Center(
                child: CircularProgressIndicator(),
              )
            : ElevatedButton(
                key: const Key('signUpForm_continue_raisedButton'),
                style: ElevatedButton.styleFrom(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(30),
                  ),
                  backgroundColor: Theme.of(context).colorScheme.secondary,
                ),
                onPressed: state.isValid && state.termsAccepted
                    ? () => context.read<SignUpCubit>().signUpFormSubmitted()
                    : null,
                child: const Text(
                  'SIGN UP',
                  style: TextStyle(color: Colors.white),
                ),
              );
      },
    );
  }
}
