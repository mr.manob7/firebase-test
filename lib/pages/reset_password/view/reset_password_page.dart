import 'package:firebase_test/pages/reset_password/cubit/reset_password_cubit.dart';
import 'package:firebase_test/pages/reset_password/view/reset_password_form.dart';
import 'package:firebase_test/repositories/repositories.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ResetPasswordPage extends StatelessWidget {
  const ResetPasswordPage({super.key});

  static Route<void> route() {
    return MaterialPageRoute<void>(builder: (_) => const ResetPasswordPage());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(title: const Text('Login')),
      backgroundColor: Colors.transparent,
      body: Padding(
        padding: const EdgeInsets.all(0),
        child: BlocProvider(
            create: (_) =>
                ResetPasswordCubit(context.read<AuthenticationRepository>()),
            child: Container(
              decoration: const BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage('assets/background.jpg'),
                      fit: BoxFit.fill)),
              child: const ResetPasswordForm(),
            )),
      ),
    );
  }
}
