import 'package:firebase_test/pages/reset_password/cubit/reset_password_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:formz/formz.dart';

class ResetPasswordForm extends StatelessWidget {
  const ResetPasswordForm({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocListener<ResetPasswordCubit, ResetPasswordState>(
      listener: (context, state) {
        if (state.status.isFailure) {
          showDialog(
            context: context,
            builder: (context) => createDialog(
                context, "User not found", "Try using different email address."),
          );
          Future.delayed(Durations.medium1, () {
            context.read<ResetPasswordCubit>().resetState();
          });
        } else if (state.status.isSuccess) {
          showDialog(
            context: context,
            builder: (context) => createDialog(
                context, "Email sent", "Check your mailbox."),
          );
          Future.delayed(Durations.medium1, () {
            context.read<ResetPasswordCubit>().resetState();
          });
        }
      },
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            margin: const EdgeInsets.only(top: 24),
            height: 64,
            padding: const EdgeInsets.only(left: 10),
            color: Colors.transparent,
            child: Row(
              children: [
                IconButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    icon: const Icon(Icons.arrow_back_ios_new))
              ],
            ),
          ),
          SingleChildScrollView(
            child: Container(
              padding: const EdgeInsets.all(20),
              decoration: const BoxDecoration(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(20),
                      topRight: Radius.circular(20)),
                  color: Color.fromARGB(255, 255, 255, 255)),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisSize: MainAxisSize.min,
                children: [
                  const SizedBox(height: 16),
                  _resetText(context),
                  const SizedBox(height: 16),
                  _emailInput(),
                  const SizedBox(height: 8),
                  _resetButton(),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

Column _resetText(context) {
  return Column(
    mainAxisAlignment: MainAxisAlignment.center,
    children: [
      Text(
        "Reset Password",
        style: TextStyle(
            color: Theme.of(context).colorScheme.secondary,
            fontSize: 30,
            fontWeight: FontWeight.bold),
      ),
    ],
  );
}

class _emailInput extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ResetPasswordCubit, ResetPasswordState>(
      buildWhen: (previous, current) => previous.email != current.email,
      builder: (context, state) {
        return TextField(
          key: const Key('reset_password_email_input'),
          onChanged: (email) =>
              context.read<ResetPasswordCubit>().emailChanged(email),
          keyboardType: TextInputType.emailAddress,
          decoration: InputDecoration(
            labelText: 'Email',
            helperText: '',
            hintText: 'Your email address',
            errorText:
                state.email.displayError != null ? 'invalid email' : null,
          ),
        );
      },
    );
  }
}

Dialog createDialog(context, String header, String message) {
  return Dialog(
    child: Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Transform(
          transform: Matrix4.translationValues(0.0, -25, 0.0),
          child: Container(
              padding: const EdgeInsets.all(15),
              decoration: BoxDecoration(
                  border: Border.all(width: 5, color: Colors.white),
                  shape: BoxShape.circle,
                  color: Colors.amber,
                  boxShadow: const [BoxShadow()]),
              child: const Icon(Icons.email)),
        ),
        Padding(
          padding: const EdgeInsets.all(20),
          child: Column(
            children: [
              Text(
                header,
                style: TextStyle(fontSize: 20),
              ),
              Text(message),
              const SizedBox(
                height: 15,
              ),
              ElevatedButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  style: ElevatedButton.styleFrom(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30),
                    ),
                    backgroundColor: Theme.of(context).colorScheme.secondary,
                  ),
                  child: const Padding(
                    padding: EdgeInsets.only(left: 30, right: 30),
                    child: Text(
                      'Okay',
                      style: TextStyle(color: Colors.white),
                    ),
                  ))
            ],
          ),
        )
      ],
    ),
  );
}

class _resetButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ResetPasswordCubit, ResetPasswordState>(
      builder: (context, state) {
        return (state.status.isInProgress)
            ? const Center(
                child: CircularProgressIndicator(),
              )
            : ElevatedButton(
                key: const Key('loginForm_continue_raisedButton'),
                style: ElevatedButton.styleFrom(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(30),
                  ),
                  backgroundColor: Theme.of(context).colorScheme.secondary,
                ),
                onPressed: state.isValid
                    ? () => context
                        .read<ResetPasswordCubit>()
                        .resetPasswordWithEmail()
                    : null,
                child: const Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Center(
                      child: Text(
                        'RESET PASSWORD',
                        style: TextStyle(color: Colors.white),
                      ),
                    )
                  ],
                ),
              );
      },
    );
  }
}
