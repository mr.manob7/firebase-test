part of 'reset_password_cubit.dart';

final class ResetPasswordState extends Equatable {
  const ResetPasswordState(
      {this.email = const Email.pure(),
      this.isValid = false,
      this.errorMessage,
      this.status = FormzSubmissionStatus.initial});

  final Email email;
  final bool isValid;
  final String? errorMessage;
  final FormzSubmissionStatus status;

  @override
  List<Object?> get props => [email, isValid, errorMessage, status];

  ResetPasswordState copyWith({
    Email? email,
    bool isValid = false,
    String? errorMessage,
    FormzSubmissionStatus? status,
  }) {
    return ResetPasswordState(
        email: email ?? this.email,
        isValid: isValid,
        errorMessage: errorMessage ?? this.errorMessage,
        status: status ?? this.status);
  }
}
