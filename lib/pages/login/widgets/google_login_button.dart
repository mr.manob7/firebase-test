import 'package:firebase_test/pages/login/cubit/login_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:formz/formz.dart';

class GoogleLoginButton extends StatelessWidget {
  const GoogleLoginButton({super.key});

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return BlocBuilder<LoginCubit, LoginState>(
      builder: (context, state) {
        return (state.status.isInProgress && state.isGoogle)
            ? const Center(
                child: CircularProgressIndicator(),
              )
            : ElevatedButton.icon(
                key: const Key('loginForm_googleLogin_raisedButton'),
                label: const Text(
                  'SIGN IN WITH GOOGLE',
                  style: TextStyle(color: Colors.white),
                ),
                style: ElevatedButton.styleFrom(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(30),
                  ),
                  backgroundColor: theme.colorScheme.secondary,
                ),
                icon: const Icon(FontAwesomeIcons.google, color: Colors.white),
                onPressed: () => context.read<LoginCubit>().logInWithGoogle(),
              );
      },
    );
  }
}
