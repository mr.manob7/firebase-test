import 'package:firebase_test/pages/login/cubit/login_cubit.dart';
import 'package:firebase_test/pages/reset_password/view/reset_password_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ForgetPasswordButton extends StatelessWidget {
  const ForgetPasswordButton({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<LoginCubit, LoginState>(
        builder: ((context, state) => Row(
              key: const Key("login_forget_password_button"),
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                TextButton(
                    onPressed: () =>
                        {Navigator.of(context).push(ResetPasswordPage.route())},
                    child: const Text("Forget Password!"))
              ],
            )));
  }
}
