import 'package:firebase_test/pages/login/cubit/login_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class PasswordInput extends StatelessWidget {
  const PasswordInput({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<LoginCubit, LoginState>(
      buildWhen: (previous, current) =>
          previous.password != current.password ||
          previous.passwordVisibility != current.passwordVisibility,
      builder: (context, state) {
        return TextField(
          key: const Key('loginForm_passwordInput_textField'),
          onChanged: (password) =>
              context.read<LoginCubit>().passwordChanged(password),
          obscureText: !state.passwordVisibility,
          decoration: InputDecoration(
              labelText: 'Password',
              helperText: '',
              hintText: 'Enter your password',
              errorText: state.password.displayError != null
                  ? 'invalid password'
                  : null,
              suffixIcon: IconButton(
                icon: Icon(state.passwordVisibility
                    ? Icons.visibility_off
                    : Icons.visibility),
                onPressed: () {
                  context.read<LoginCubit>().togglePasswordVisibility();
                },
              )),
        );
      },
    );
  }
}
