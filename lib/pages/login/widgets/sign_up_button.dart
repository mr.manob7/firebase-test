import 'package:firebase_test/pages/sign_up/sign_up.dart';
import 'package:flutter/material.dart';

class SignUpButton extends StatelessWidget {
  const SignUpButton({super.key});

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        const Text(
          "Don't have an account?",
        ),
        TextButton(
            onPressed: () => {Navigator.of(context).push(SignUpPage.route())},
            child: const Text('SIGN UP'))
      ],
    );
  }
}
